
;;; Some average/elasticity things

(defun average (&rest nums) (/ (reduce #'+ nums) (length nums)))
(defun elastic (q1 q2 x1 x2) (/ (/ (- q2 q1) (average q1 q2)) (/ (- x2 x1) (average x1 x2))))

;;; Matracies

(defun col (matrix index)
  (let ((out (list)))
    (dolist (x matrix)
      (setf out (cons (elt x index) out)))
    (reverse out)))

(defun row (matrix index)
  (elt matrix index))
